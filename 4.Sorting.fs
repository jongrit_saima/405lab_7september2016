﻿let rec SubArrayBegin list n =
    list |> Seq.ofList |> Seq.take n |> List.ofSeq



let SubArrayEnd list n =     
    List.rev (SubArrayBegin (List.rev list) ((List.length list) - n - 1))


let InsertSort list = 
    let rec innerInsertSort list n = 
        if n = (List.length list) then
            list
        else
            let key = List.nth list n
            let firstList = SubArrayBegin list n
            let (firstListA, firstListB) = List.partition (fun x -> x < key) firstList
            let remList = SubArrayEnd list n
            innerInsertSort (firstListA @ [key] @ firstListB @ remList) (n + 1)
    innerInsertSort list 1

[<EntryPoint>]
let main args = 
    let a = [10; 2; 9; 1; 4; 5; -1] //number
    let b = InsertSort a
    List.iter (fun x -> printfn "%i" x) b
    0
